package cmd

// CommandProcessor Processor abstraction
type CommandProcessor interface {
	Name() string
	Execute()
}

type CommandProcessorFactory struct {
	processors map[string] CommandProcessor
}

func (f *CommandProcessorFactory) Add(p CommandProcessor) {
	f.processors[p.Name()] = p
}

func (f *CommandProcessorFactory) Processor(name string) CommandProcessor {
	return f.processors[name]
}

func (f *CommandProcessorFactory) Processors() []CommandProcessor {
	processors := make([]CommandProcessor, 0)

	for _, value := range f.processors {
		processors = append(processors, value)
	}

	return processors
}

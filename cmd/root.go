package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)


var processorFactory = CommandProcessorFactory{processors: make(map[string]CommandProcessor)}


var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Generate a project using a config file named (aldo.json)",
	Run: func(cmd *cobra.Command, args []string) {
		processorFactory.Processor("init").Execute()
	},
}

var rootCmd = &cobra.Command{
	Use:   "aldo [command] --flags",
	Short: "Make it easy to setup software projects",
}

func configureRootCommands() {
	rootCmd.AddCommand(initCmd)
}

func configureCommandProcessorFactory() {

	processorFactory.Add(&InitCommandProcessor{})
}
// Execute Execute the command
func Execute() {

	// Init command processors factory
	configureCommandProcessorFactory()

	// Init rood command
	configureRootCommands()
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

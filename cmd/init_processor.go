package cmd

import (
	"gitlab.com/boriswaguia/aldo/actions/generator"
	"gitlab.com/boriswaguia/aldo/config"
	"log"
	"os"
	"strings"
)

type InitCommandProcessor struct {

}

func (p *InitCommandProcessor) Name() string {
	return "init"
}

func (p *InitCommandProcessor) Execute() {
	// get current path
	dir, err := os.Getwd()
	if err != nil {
		log.Panic(err)
	}
	if !strings.HasSuffix(dir, "/") {
		dir = strings.Join(append([]string{}, dir, "/"), "")
		project, err2 := config.LoadProjectData(dir + "aldo.json")
		if err2 != nil {
			log.Panic(err2)
		}
		generator.ScaffoldProject(project, dir)
	}
}

package config

import "testing"

func TestReadSampleToModel(t *testing.T) {
	project, err := LoadProjectData("sample.json")
	if err != nil {
		t.Error(err)
	}
	if project == nil {
		t.Errorf("Unable to read config file")
	}
}

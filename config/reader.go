package config

import (
	"io/ioutil"

	"encoding/json"
)

// LoadProjectData Read config file and return data configuration
func LoadProjectData(filePath string) ([]Project, error) {
	return loadDataInternal(filePath)
}

func loadDataInternal(filePath string) ([]Project, error) {
	buf, err := ioutil.ReadFile(filePath)

	if err != nil {
		return nil, err
	}
	// project := NewProject()
	projects := make([]Project, 0)
	json.Unmarshal(buf, &projects)
	return projects, nil
}

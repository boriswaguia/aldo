package config

type ProjectType string

const (
	MAVEN ProjectType = "maven"
	ANGULAR ProjectType = "angular"
)

type Project struct {
	Type	 ProjectType `json:type`
	ID       string    `json:"id"`
	Projects []Project `json:"projects"`
}

package angular

import (
	"fmt"
	"gitlab.com/boriswaguia/aldo/config"
)

type Generator struct {}

func (g *Generator) ProjectType() config.ProjectType {
	return config.ANGULAR
}

func (g *Generator) Generate(project config.Project, dir string, parentID string) {
	fmt.Println("Generating Angular Project", project, dir, parentID)
}

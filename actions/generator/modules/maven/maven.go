package maven

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/boriswaguia/aldo/config"
)

// MavenGenerator
type Generator struct {}


func (g *Generator) ProjectType() config.ProjectType {
	return config.MAVEN
}

func (g *Generator) Generate(project config.Project, dir string, parentID string) {
	scaffoldMavenProject(project, dir, parentID)
}

func hasSubProjects(project config.Project) bool {
	return project.Projects != nil && len(project.Projects) > 0
}

// ResourceType resource type. 0 for files, 1 for folder, other values should not be considered
type ResourceType int

// ProjectResource Project resource
type ProjectResource struct {
	Path    string
	Content string
	Type    ResourceType
}

// generatePom Generate a Pom file
func generatePom(groupID string, artifactID string, version string, packaging string) string {
	template := `<?xml version="1.0" encoding="UTF-8"?>

	<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	  <modelVersion>4.0.0</modelVersion>
	
	  <groupId>%v</groupId>
	  <artifactId>%v</artifactId>
	  <version>%v</version>
	  <packaging>%v</packaging>
	
	  <name>quicktest</name>
	  <!-- FIXME change it to the project's website -->
	  <url>http://www.example.com</url>
	
	  <properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.source>1.7</maven.compiler.source>
		<maven.compiler.target>1.7</maven.compiler.target>
	  </properties>
	
	  <dependencies>
		<dependency>
		  <groupId>junit</groupId>
		  <artifactId>junit</artifactId>
		  <version>4.11</version>
		  <scope>test</scope>
		</dependency>
	  </dependencies>
	
	  <build>
		<pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
		  <plugins>
			<!-- clean lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#clean_Lifecycle -->
			<plugin>
			  <artifactId>maven-clean-plugin</artifactId>
			  <version>3.1.0</version>
			</plugin>
			<!-- default lifecycle, jar packaging: see https://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
			<plugin>
			  <artifactId>maven-resources-plugin</artifactId>
			  <version>3.0.2</version>
			</plugin>
			<plugin>
			  <artifactId>maven-compiler-plugin</artifactId>
			  <version>3.8.0</version>
			</plugin>
			<plugin>
			  <artifactId>maven-surefire-plugin</artifactId>
			  <version>2.22.1</version>
			</plugin>
			<plugin>
			  <artifactId>maven-jar-plugin</artifactId>
			  <version>3.0.2</version>
			</plugin>
			<plugin>
			  <artifactId>maven-install-plugin</artifactId>
			  <version>2.5.2</version>
			</plugin>
			<plugin>
			  <artifactId>maven-deploy-plugin</artifactId>
			  <version>2.8.2</version>
			</plugin>
			<!-- site lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#site_Lifecycle -->
			<plugin>
			  <artifactId>maven-site-plugin</artifactId>
			  <version>3.7.1</version>
			</plugin>
			<plugin>
			  <artifactId>maven-project-info-reports-plugin</artifactId>
			  <version>3.0.0</version>
			</plugin>
		  </plugins>
		</pluginManagement>
	  </build>
	</project>`
	content := fmt.Sprintf(template, groupID, artifactID, version, packaging)
	return content
}

func generateMainJava(packageName string) string {
	return fmt.Sprintf(`package %v;

	/**
	 * Hello world!
	 *
	 */
	public class App 
	{
		public static void main( String[] args )
		{
			System.out.println( "Hello World!" );
		}
	}`, packageName)
}

func generateMainTestJava(packageName string) string {
	return fmt.Sprintf(`
		package %v;

		import static org.junit.Assert.assertTrue;

		import org.junit.Test;

		/**
		* Unit test for simple App.
		*/
		public class AppTest 
		{
			/**
			* Rigorous Test :-)
			*/
			@Test
			public void shouldAnswerWithTrue()
			{
				assertTrue( true );
			}
		}
		`, packageName)
}
func generateFiles(projectID string, dir string, hasSubProject bool) {
	// meta[0] is groupeId : com.gitlab.boriswaguia
	// meta[1] is artifact id : sample-user
	// meta[2] is version
	err := os.MkdirAll(dir, os.ModePerm)
	if err != nil {
		log.Panic(err)
	}
	meta := strings.Split(projectID, ":")

	packageName := strings.ReplaceAll(meta[0], ".", "/") + "/" + strings.ReplaceAll(meta[1], "-", "")

	packaging := "pom"
	if !hasSubProject {
		packaging = "jar"
	}
	pom := ProjectResource{Path: dir + "pom.xml", Content: generatePom(meta[0], meta[1], meta[2], packaging), Type: 0}
	files := []ProjectResource{pom}

	if !hasSubProject {
		src := ProjectResource{Path: dir + "src/", Type: 1}
		packageJava := ProjectResource{Path: src.Path + "main/java/" + packageName, Type: 1}
		packageTest := ProjectResource{Path: src.Path + "test/java/" + packageName, Type: 1}
		mainJava := ProjectResource{Path: packageJava.Path + "/" + "Main.java", Content: generateMainJava(meta[0] + "." + strings.ReplaceAll(meta[1], "-", "")), Type: 0}
		mainTest := ProjectResource{Path: packageTest.Path + "/" + "MainTest.java", Content: generateMainTestJava(meta[0] + "." + strings.ReplaceAll(meta[1], "-", "")), Type: 0}
		files = append(files, src, packageJava, packageTest, mainJava, mainTest)
	}

	for i := 0; i < len(files); i++ {
		file := files[i]
		if file.Type == 0 {
			err := ioutil.WriteFile(file.Path, bytes.NewBufferString(file.Content).Bytes(), 0644)
			if err != nil {
				log.Panic(err)
			}
			fmt.Println("created file", file.Path)
		}

		if file.Type == 1 {
			err := os.MkdirAll(file.Path, os.ModePerm)
			if err != nil {
				log.Panic(err)
			}
			fmt.Println("created dir", file.Path)
		}
	}
}

// ScaffoldMavenProject Scaffold a maven project
func scaffoldMavenProject(project config.Project, dir string, parentID string) {
	if !strings.HasSuffix(dir, "/") {
		dir = dir + "/"
	}
	hasSubProjects := hasSubProjects(project)
	generateFiles(project.ID, dir, hasSubProjects)
	if hasSubProjects {
		for i := 0; i < len(project.Projects); i++ {
			subProject := project.Projects[i]

			subDir := strings.Split(subProject.ID, ":")[1]
			scaffoldMavenProject(subProject, fmt.Sprintf(dir+"%v", subDir), project.ID)
		}
	}
}

package maven

import "testing"

import "gitlab.com/boriswaguia/aldo/config"

import "os"

func TestScaffoldMavenProject(t *testing.T) {
	subProject := newProject("com.gitlab.boriswaguia:sampletest-user:default", make([]config.Project, 0))
	project := config.Project{ID: "com.gitlab.boriswaguia:sampletest:default", Projects: []config.Project{subProject}}
	var parentID string
	generator := &MavenGenerator{}
	generator.Generate(project, os.TempDir(), parentID)
}

func newProject(id string, subProjects []config.Project) config.Project {
	return config.Project{ID: id, Projects: subProjects}
}

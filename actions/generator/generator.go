package generator

import (
	"fmt"
	"gitlab.com/boriswaguia/aldo/actions/generator/modules/angular"
	"gitlab.com/boriswaguia/aldo/actions/generator/modules/maven"
	"gitlab.com/boriswaguia/aldo/config"
	"log"
)

type Generator interface {
	ProjectType() config.ProjectType
	Generate(project config.Project, dir string, parentID string)
}

// Registry Holds a grip of all Generators
type Registry struct {
	generators map[config.ProjectType] Generator
}

func (registry *Registry) Get(projectType config.ProjectType) Generator {
	return registry.generators[projectType]
}

func (registry *Registry) Add(generator Generator) {
	registry.generators[generator.ProjectType()] = generator
}


func registerGenerators(generators ...Generator) Registry {
	registry := Registry{generators: make(map[config.ProjectType]Generator)}

	for _, generator := range generators {
		registry.Add(generator)
	}
	return registry
}


// ScaffoldProject Generate projects folders
func ScaffoldProject(projects []config.Project, dir string) {
	registry := registerGenerators(&maven.Generator{}, &angular.Generator{})

	var parentProjectID string

	for _, project := range projects {
		// call registry and apply
		if generator := registry.Get(project.Type); generator != nil {
			generator.Generate(project, dir, parentProjectID)
		} else {
			log.Panic(fmt.Sprintf(`No generator found for project type - "%v"`, project.Type))
		}
	}

}

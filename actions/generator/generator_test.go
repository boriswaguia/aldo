package generator

import "testing"

import "gitlab.com/boriswaguia/aldo/config"

import "fmt"

import "os"

func TestSimpleProjectStructureGenerated(t *testing.T) {
	project, err := config.LoadProjectData("sample.json")
	if err != nil {
		fmt.Println(err)
	}
	ScaffoldProject(project, os.TempDir())
}
